# Pipeline Secrets

Pipeline Secrets is a Java Spring Boot application that lets users manage secrets in their CI/CD pipeline from a single application. With
Pipeline Secrets, users can view, edit, and delete secrets for all their pipeline stages in one convenient interface, making it easier to manage and
secure their pipeline.

## Getting Started

To get started with Pipeline Secrets, follow these steps:

1. Clone the repository: `git clone https://github.com/yourusername/pipeline-secrets.git`
2. Install the necessary dependencies: `mvn install`
3. Start the application: `mvn spring-boot:run`
4. Once the application is running, you can access it by navigating to `http://localhost:8080` in your web browser.

## Usage

To use Pipeline Secrets, users must first authenticate with the application using their CI/CD platform credentials. Once authenticated, they can view
a
list of all their pipeline stages and the secrets associated with each stage. They can then edit or delete individual secrets as needed.

Pipeline Secrets also provides an API for programmatic access to secrets. Users can use the API to retrieve secrets for use in their pipeline scripts,
or to create new secrets programmatically.

## Configuration

Pipeline Secrets can be configured using environment variables or a application.yml file. The following configuration options are available:

- `server.port`: The port on which the application will run (default: `8080`).
- `spring.datasource.url`: The URL for the database connection.
- `spring.datasource.username`: The username for the database connection.
- `spring.datasource.password`: The password for the database connection.
- `spring.jpa.hibernate.ddl-auto`: The Hibernate DDL mode (default: update).
- `spring.jpa.show-sql`: Whether to show SQL statements in the logs (default: false).
- `spring.jackson.serialization.INDENT_OUTPUT`: Whether to enable pretty-printing of JSON (default: true).

## Contributing

If you'd like to contribute to Pipeline Secrets, feel free to fork the repository and submit a pull request. Before submitting a pull request, please
make sure that your changes are fully tested and that they conform to the existing code style and standards.

## License

Pipeline Secrets is released under the MIT license. See LICENSE for more information.
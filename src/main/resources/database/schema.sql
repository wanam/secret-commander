CREATE DATABASE secret_commander;
USE secret_commander;

CREATE TABLE users
(
    id         BIGINT PRIMARY KEY AUTO_INCREMENT,
    username   VARCHAR(50)  NOT NULL,
    password   VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE ci_cd_platforms
(
    id         BIGINT PRIMARY KEY AUTO_INCREMENT,
    name       VARCHAR(50)  NOT NULL,
    type       VARCHAR(50)  NOT NULL,
    api_key    VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE secrets
(
    id                BIGINT PRIMARY KEY AUTO_INCREMENT,
    name              VARCHAR(50)  NOT NULL,
    value             VARCHAR(100) NOT NULL,
    ci_cd_platform_id BIGINT       NOT NULL,
    created_at        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (ci_cd_platform_id) REFERENCES ci_cd_platforms (id) ON DELETE CASCADE
);

CREATE TABLE access_tokens
(
    id                BIGINT PRIMARY KEY AUTO_INCREMENT,
    token             VARCHAR(100) NOT NULL,
    user_id           BIGINT       NOT NULL,
    ci_cd_platform_id BIGINT       NOT NULL,
    created_at        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    FOREIGN KEY (ci_cd_platform_id) REFERENCES ci_cd_platforms (id) ON DELETE CASCADE
);
